<?php
	class conectorMySQL {
		protected $conn;
		protected $filasAfectadas=0;
		public function __construct() {
			$servername = "localhost";
			$username = "root";
			$password = "";
			$db = "test";
			try {
				$this->conn = new PDO("mysql:host=$servername;dbname=".$db, $username, $password);
				// GENERAR MODO PARA EXCEPTION
				$this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			}catch(PDOException $e){
				//SALIDA ERROR
				echo "Conexión fallida: " . $e->getMessage();
			}
		}

		public function __destruct(){}

		public function query($sql){
			//RETORNA true SI SE EJECUTÓ CON ÉXITO SQL, false SI NO LO HIZO
			$this->filasAfectadas=intval($this->conn->exec($sql));
			if ($this->filasAfectadas>0) {
				return (true);
			}else{
				return (false);
			}
		}

		public function idInsertado(){
			return ($this->conn->lastInsertId());
		}

		public function consultar($sql){
			$stmt = $this->conn->prepare($sql);
			$stmt->execute();
			return ($stmt->fetchAll());
		}

		public function filasAfectadas(){
			return ($this->filasAfectadas);
		}
	}

//$m=new conectorMySQL();
/*
	$sql = "INSERT INTO usuarios (usuario_id, user, password, ultimo_acceso) VALUES (1,'Juanillo','0123456789',NOW())";
	$m->query($sql);
*/

/*
	//$sql = "DELETE FROM usuarios WHERE id=16";
	$m->query($sql);
*/

/*
	//$sql = "UPDATE usuarios SET ultimo_acceso=NOW() WHERE id IN(16,17,18,19,20)";
	$m->query($sql);
*/

/*
	//CANTIDAD DE FILAS MODIFICADAS/ACTUALIZADAS/ELIMINADAS DE UNA CONSULTA (EXCEPTO consultar())
	$m->filasAfectadas();
*/

/*
	$sql = "SELECT * FROM usuarios";
	$resultados=$m->consultar($sql);
	foreach ($resultados as $resultado) {
		var_dump($resultado["id"]);
		echo "<br><br>";
	}
*/

?>