<!DOCTYPE html>
<html>
<head>
	<title>Acceso a Arrazola</title>
	<?php include_once './librerias.php'; ?>
	<script type="text/javascript">
		$(document).ready(function(){
			if (<?php if(isset($_GET["error"])) echo 1; else echo 0; ?>==1) {
				$("#error").append("Error. Email y/o contraseña no válidos.");
				$("#error").show();
			}
		});
		
	</script>
</head>
<body>
	<div class="container">
		<div class="col-md-4 col-md-offset-4">
			<form class="form-signin" method="POST" action="./php/login.php">
		        <h2 class="form-signin-heading">Arrazola</h2>
		        <label for="email" class="sr-only">Email address</label>
		        <input type="email" id="email" class="form-control" placeholder="Correo Electrónico" name="email" required autofocus value="fireangelmg1@hotmail.com">
		        <label for="password" class="sr-only">Password</label>
		        <input type="password" id="password" class="form-control" placeholder="Password" name="password" required value="0203">
		        <br>
		        <button class="btn btn-lg btn-primary btn-block" type="submit">Ingresar</button>
		    </form>
		    <br>
		    <?php include_once './mensajeServidor.php'; ?>
		</div>	      
    </div>
</body>
</html>