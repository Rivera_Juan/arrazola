<div class="alert alert-success" hidden id="exito">
  <strong>Success!</strong>
</div>

<div class="alert alert-info" hidden id="info">
  <strong>Info!</strong>
</div>

<div class="alert alert-warning" hidden id="advertencia">
  <strong>Warning!</strong>
</div>

<div class="alert alert-danger" hidden id="error">
  <strong>Danger!</strong>
</div>